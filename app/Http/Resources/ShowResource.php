<?php

namespace App\Http\Resources;

use App\Models\casos;
use App\Models\CCAAs;
use App\Models\ia14;
use App\Models\ia7;
use App\Models\muertos;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function show($id)
    {

        $ia14 = ia14::where("fecha",$id)->first();
        $ia7 = ia7::where("fecha",$id)->first();
        $casos = casos::where("fecha",$id)->first();
        $muertos = muertos::where("fecha",$id)->first();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        if($ia14){
            return new ShowResource($ia14);
        }
        if($ia7){
            return new ShowResource($ia7);
        }
        if($casos){
            return new ShowResource($casos);
        }
        if($muertos){
            return new ShowResource($muertos);
        }
    }

    public function toArray($request)
    {

        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')
            ->first();
        $ccaa = CCAAs::where('id', $this->id_ccaa)->first();
        if(!isset($this->incidencia)){
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'media' => $this->numero
            ];
        }else{
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'media' => $this->incidencia
            ];
        }
    }

}
